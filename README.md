# PBconvert Script - README #
---

### Overview ###

**PBconvert** is a small Bash script which converts MP4 video files to MP3 sound files using the '**ffmpeg**' and '**lame**' packages.

### Screenshot ###

![PBconvert - MP4 to MP3 conversion](development/readme/pbconvert1.png "PBconvert - MP4 to MP3 conversion")

### Setup ###

* Copy the directory **pbconvert** to your computer.
* Open the script **convert_mp4_to_mp3.sh** in your favorite text editor and customize the configuration section.
* Make the script executable: **chmod +x convert_mp4_to_mp3.sh**
* Change to the directory of the script: **cd /path/to/script/pbconvert**
* Copy some MP4 video files to the input directory **pbconvert/input**.
* Run the script from the command line: **./convert_mp4_to_mp3.sh**
* Get the converted MP3 sound files from the output directory **pbconvert/output**.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBconvert** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
